from marshmallow import Schema, fields, validate


class PostSearchSchema(Schema):
    username = fields.Str()
    pattern = fields.Str(validate=validate.Regexp)
