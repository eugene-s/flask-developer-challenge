# coding=utf-8
"""
Exposes a simple HTTP API to search a users Gists via a regular expression.

Github provides the Gist service as a pastebin analog for sharing code and
other develpment artifacts.  See http://gist.github.com for details.  This
module implements a Flask server exposing two endpoints: a simple ping
endpoint to verify the server is up and responding and a search endpoint
providing a search across all public Gists for a given Github account.
"""
import asyncio
import re
from httpx import AsyncClient, HTTPStatusError
from flask import Flask, jsonify, request
from aiocache import cached

from .schemas import PostSearchSchema


# *The* app object
app = Flask(__name__)


@app.route("/ping")
async def ping():
    """Provide a static response to a simple GET request."""
    return "pong"


async def gists_for_user(
    username, max_page=10, per_page=50
):  # todo: move default values to settings
    """Provides the list of gist metadata for a given user.

    This abstracts the /users/:username/gist endpoint from the Github API.
    See https://developer.github.com/v3/gists/#list-a-users-gists for
    more information.

    Args:
        username (string): the user to query gists for

    Returns:
        The dict parsed from the json response from the Github API.  See
        the above URL for details of the expected structure.
    """
    async with AsyncClient() as client:
        # iterating pages until a page is empty
        for page in range(max_page):
            gists_url = (
                f"https://api.github.com/users/{username}/gists?"
                f"per_page={per_page}&page={page + 1}"
            )
            response = await client.get(gists_url)
            # throw errors if it is
            response.raise_for_status()
            data = response.json()
            for item in data:
                yield item
            if len(data) < per_page:
                break



async def search_in_gist(files, pattern):
    async with AsyncClient() as client:
        regex = re.compile(pattern, re.MULTILINE)
        tasks = [client.get(file["raw_url"]) for file in files.values()]
        results = await asyncio.gather(*tasks)
        for res in results:
            if regex.search(res.content.decode()):
                return True
        return False


@cached(
    ttl=3600,
    key_builder=lambda f, username, *args, **kwargs: f"{f.__name__}_{username}",
)  # applied simple cache, but on production better to use Redis/Memcached etc
async def search_gists(username, pattern):
    matches = []
    async for gist in gists_for_user(username):
        if await search_in_gist(gist["files"], pattern):
            matches.append(gist["url"])
        # BONUS: What about huge gists?
        # huge gists should be processed in parallel. Depends on system requires,
        # we can create multiple workers which will fetch gists and check
        # them simultaneously
    return matches


@app.route("/api/v1/search", methods=["POST"])
async def search():
    """Provides matches for a single pattern across a single users gists.

    Pulls down a list of all gists for a given user and then searches
    each gist for a given regular expression.

    Returns:
        A Flask Response object of type application/json.  The result
        object contains the list of matches along with a 'status' key
        indicating any failure conditions.
    """
    data = PostSearchSchema().load(request.get_json())
    try:
        matches = await search_gists(data["username"], data["pattern"])
    except HTTPStatusError as exc:
        status_class = exc.response.status_code // 100
        if status_class == 4:
            error = exc.response.json()
            error.pop("documentation_url", None)
            return jsonify(error)

    data["status"] = "success"
    data["matches"] = matches

    return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=9876)
